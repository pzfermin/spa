//Single Page Application / SPA

let links = document.querySelectorAll("a");
let main = document.querySelector("main")


//Recorremos el arreglo link que contiene las referencias de las paginas
links.forEach((link)=>{
    //A cada link le agregamos una escucha de evento
    link.addEventListener("click",(e)=>{
        e.preventDefault();
        //Asigno a id la propiedad id de cada objeto
        let id = e.target.id
        //Solo cambio el hash para poder usarlo mas adelante a mi favor
        location.hash=link.id;
        })
})

//Hago exactamente los mismo que arriba pero con una precarga del Home para que cuando se ingrese o se recargue la pagina lo primero que aparezca 
//sea el contenido de home.
let pagina_incial = ajax("home.html");
pagina_incial.addEventListener("load",()=>{
    if(pagina_incial.status == 200){
        main.innerHTML=pagina_incial.response
    }
})



function ajax(url,metodo){
    //Si no recibe el parametro metodo entonces es "GET"
    let http_metodo = metodo || "GET";
    //Asignamos a xhr el objeto XMLHttpRequest
    let xhr = new XMLHttpRequest
    //Le decimos al objeto xhr que utilice el metodo open y le pasamos por parametro
    //la variable http_metodo y url
    xhr.open(http_metodo,url)
    xhr.send()
    //Retorno el objeto xhr entero ya que no se puede retornar la respuesta
    return xhr
}

window.addEventListener("hashchange",()=>{
    //tomo solo la parte de hash que me sirve para identificar el archivo
    let archivo = location.hash.split("#")[1] + ".html"
    //le asigno a xhr la funcion ajax y le paso por parametro la variable archivo que contiene el nombre del archivo que tiene que ir a buscar
    let xhr = ajax(archivo)
    //Quedo a la escucha de que se termine de cargar la funcion ajax y cuando este cargada se agrega al index la respuesta del pedido asincrono
    xhr.addEventListener("load", ()=>{
        if(xhr.status == 200){
            main.innerHTML = xhr.response                 
        }
    })

})







